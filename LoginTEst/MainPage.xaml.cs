﻿using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Authentication.Web;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace LoginTEst
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly MsalAuthHelper _msalHelper = new MsalAuthHelper("76ec8043-6991-4671-a80e-1fee1086e49c");
        private IUser user = null;
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
           // var authorizationUri = new Uri("https://qa-id.test.subway.com/28fe2ff7-021a-4e9d-a713-1baa341c71d7/oauth2/v2.0/authorize?p=b2c_1a_signup_signin_common-v2&scope=+offline_access+openid+profile&response_type=code&client_id=76ec8043-6991-4671-a80e-1fee1086e49c&redirect_uri=com.subway.kiosk%3A%2F%2Fsubway%2Fkiosk%2F&client-request-id=6b1f971f-d275-49c1-b610-afc066b0efcd&x-client-SKU=MSAL.WinRT&x-client-Ver=1.1.0.0&x-client-CPU=x64&x-client-DM=XPS+8500&prompt=login&code_challenge=_DlLB-r9nTSLG3VetfZoehWV4bfoxf3W0qxypdN8kEE&code_challenge_method=S256&state=533c7534-adc2-4c68-94a8-0a9c2f7f3150");
            user = await _msalHelper.SignIn().ConfigureAwait(false);
           /* var result= await WebAuthenticationBroker
                .AuthenticateAsync(Windows.Security.Authentication.Web.WebAuthenticationOptions.None,authorizationUri, new Uri("com.subway.kiosk://subway/kiosk/"))
                .AsTask()
                .ConfigureAwait(false);
                */
            Debug.WriteLine(user);

        }
    }
}
